#!/bin/bash

IMAGE_NAME=${PWD##*/}

docker build -t wavefier/${IMAGE_NAME} .

docker run -it -v $PWD:/wth wavefier/wavefier-trigger-handlers:latest
