
***********************
How to use this modules
***********************



This module can be used in two different ways: as a foo library to include in your project, or as a script to launch in the shell like Command line

Library Interface
=================

The library provides different utilities, but mainly provides the mechanism to send and receive triggers from the wavefier system

TriggerDistributor
------------------

The class name is :class:`TriggerDistributor.TriggerDistributor` allows you to send triggers from your wavefier system. Below a example of it

 .. code:: python

    from wavefier_trigger_handlers.TriggerDistributor import TriggerDistributor
    from wavefier_common.param.WdfParam import WdfParam
    from wavefier_common.param.WitheningParam import WitheningParam
    from wavefier_common.Trigger import Trigger


    if __name__ == '__main__':

        broker_host = "kafka.mylan.lan"
        broker_port = 9092

        channel = "unit_test01"
        wdfParam = WdfParam()
        wthParam = WitheningParam()

        trigger = Trigger()

        triggerDistributor = TriggerDistributor(broker_host, broker_port)

        triggerDistributor.deploy(channel, wthParam, wdfParam, trigger)




TriggerSupplier
---------------

The class name is :class:`TriggerSupplier.TriggerSupplier` allows you to receive triggers from your wavefier system. Below a example of it


 .. code:: python

    class MyExampleOfListener(TriggerListener):

        def newTriggerAppears(self, trigger: WavefierTriggerMessage):
            print(trigger)


    if __name__ == '__main__':

        broker_host = "kafka.mylan.lan"

        broker_port = 9092

        channel = "unit_test01"

        iMyListener = MyExampleOfListener()

        trigger_supplier = TriggerSupplier(broker_host, broker_port)

        trigger_supplier.addTriggerListener(iMyListener)

        trigger_supplier.startObserving(channel)




Command Line Interface
======================

Below are all the possible commands installed from the library:

- ``python wavefier_trigger_handler/cli/InfluxDBImporterDaemon.py --help``
- ``python wavefier_trigger_handler/cli/CSVImporter.py --help``
- ``python wavefier_trigger_handler/cli/CSV2Influxdb.py --help``


InfluxDBImporterDaemon
----------------------

The python script to run is ``InfluxDBImporterDaemon.py`` attach the kafka topic, listen triggers and import all data in to InfluxDB.


.. code:: bash

    Usage: influxdb-importer-daemon [OPTIONS]

    Options:
      -sc, --source-channel TEXT      The source channel where the trigger is
                                      calculated
      -ih, --influxdb-host TEXT       The InfluxDB host where store the Trigger
      -ip, --influxdb-port INTEGER    he InfluxDB port where store the Trigger
      -idb, --influxdb-db-name TEXT   he InfluxDB database hame where store the
                                      Trigger
      -kbh, --kafka-broker-host TEXT  The kafka broker host where listener the
                                      Trigger
      -kbp, --kafka-broker-port INTEGER
                                      The kafka broker port where listener the
                                      Trigger
      --log-level [DEBUG|INFO|WARNING|ERROR|CRITICAL]
                                      Verbose or not verbose computation  [default:
                                      INFO]
      --help                          Show this message and exit.

Example:

.. code:: bash

    python wavefier_trigger_handler/cli/InfluxDBImporterDaemon.py -sc Hrec_hoft_16384Hz_O2Replay2 --log-level DEBUG -kbh 10.0.0.10


CSVImporter
-----------

The python script to run is ``CSVImporter.py`` Import CSV trigger directly on kafka cloud.


.. code:: bash

    Usage: CSVImporter.py [OPTIONS] FILENAME

    Options:
      -sc, --source-channel TEXT      The source channel where the trigger is
                                      calculated
      -ar, --ar INTEGER               The AR parameter  [default: 3000]
      -ws, --window-size INTEGER      Windows Size  [default: 1024]
      -wo, --window-overlap INTEGER   Window Overlap  [default: 16]
      -snr, --snr FLOAT               the mininum SNR threshould  [default: 4.0]
      -kbh, --kafka-broker-host TEXT  The kafka broker host where listener the
                                      Trigger  [default: 34.245.53.118]
      -kbp, --kafka-broker-port INTEGER
                                      The kafka broker port where listener the
                                      Trigger  [default: 9092]
      --log-level [DEBUG|INFO|WARNING|ERROR|CRITICAL]
                                      Verbose or not verbose computation
                                      [default: WARNING]
      --help                          Show this message and exit.

Example:

.. code:: bash

    python wavefier_trigger_handler/cli/CSV2Influxdb.py -sc Hrec_hoft_16384Hz_O2Replay2 --log-level INFO ./WDFTrigger-V1:Hrec_hoft_16384Hz-GPS1223412073-AR3000-Win1024-Ov16-SNR4.csv



CSV to Influxdb
---------------

The python script to run is ``CSV2Influxdb.py`` Import CSV trigger directly on InfluxDB without going through the kafka cloud.


.. code-block:: bash

    Usage: CSV2InfluxDB.py [OPTIONS] FILENAME

    Options:
      -sc, --source-channel TEXT      The source channel where the trigger is
                                      calculated  [default:
                                      V1:Hrec_hoft_16384Hz_O2Replay2]
      -ar, --ar INTEGER               The AR parameter  [default: 3000]
      -ws, --window-size INTEGER      Windows Size  [default: 1024]
      -wo, --window-overlap INTEGER   Window Overlap  [default: 16]
      -snr, --snr INTEGER             the mininum SNR threshould  [default: 4]
      -ih, --influxdb-host TEXT       The InfluxDB host where store the Trigger
                                      [default: 192.168.178.47]
      -ip, --influxdb-port INTEGER    he InfluxDB port where store the Trigger
                                      [default: 8086]
      -idb, --influxdb-db-name TEXT   he InfluxDB database hame where store the
                                      Trigger  [default: trigger_report_db]
      --log-level [DEBUG|INFO|WARNING|ERROR|CRITICAL]
                                      Verbose or not verbose computation
                                      [default: WARNING]
      --help                          Show this message and exit.


Example:

.. code:: bash

    python wavefier_trigger_handler/cli/CSV2Influxdb.py -sc Hrec_hoft_16384Hz_O2Replay2 --log-level INFO  --influxdb-host 10.0.0.1 --influxdb-db-name trigger ./WDFTrigger-V1:Hrec_hoft_16384Hz-GPS1223412073-AR3000-Win1024-Ov16-SNR4.csv
