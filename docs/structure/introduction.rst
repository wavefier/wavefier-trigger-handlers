************
Introduction
************

The wavefier_trigger_handler library is part of the Wavefier project.
This module contains all the code to send and receive the Triggers on wavefier system.
This module can be used in two different ways: as a foo library to include in your project, or as a script to launch in the shell like Command line
This library provided us with a service that is able to fill a influxDB database and create graphs and reports using the Grafana tool

.. image:: ../_static/img/WavefierTriggerHandlers.png
   :scale: 100%
   :alt: Simple schema of it
   :align: center







