Welcome to wavefier_trigger_handler's documentation!
====================================================
The wavefier_trigger_handler library is part of the Wavefier project. This module contains all the code to send and receive the Triggers on wavefier system.


.. image:: _static/img/WavefierTriggerHandlers.png
   :scale: 80%
   :alt: alternate text
   :align: center


Table of content
----------------

.. toctree::
   :maxdepth: 2

   structure/introduction
   structure/installation
   structure/use_it
   structure/develop_it
   structure/apidoc

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`