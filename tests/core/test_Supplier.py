import logging
import os
import shutil
from unittest import TestCase

import tests
from wavefier_trigger_handlers.core.Supplier import Supplier
from wavefier_trigger_handlers.core.SupplierTopicAdapter import SupplierTopicAdapter

class SupplierTest(TestCase):

    def test_new(self):
        sup = Supplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port)
        self.assertIsNotNone(sup)

    def test_start(self):
        sup = Supplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port,timeout=3)
        iDummyMessage= SupplierTopicAdapter("channel_test")
        sup.startObserving(iDummyMessage)

        self.assertIsNotNone(sup)

        sup.stopObserving()

    def test_stop(self):
        sup = Supplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port, timeout=3)
        iDummyMessage= SupplierTopicAdapter("channel_test")
        sup.startObserving(iDummyMessage)
        sup.stopObserving()

        self.assertIsNotNone(sup)
