import os
from unittest import TestCase

import shutil

from wavefier_trigger_handlers.core.WavefierTriggerMessage import WavefierTriggerMessage
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.kafka.Consumer import KafkaConsumer
from wavefier_common.kafka.Producer import KafkaProducer


class WavefierTriggerMessageTest(TestCase):

    def test_creation_obect(self):
        obj = WavefierTriggerMessage("unit_test", Trigger(), WdfParam(), WitheningParam())
        self.assertIsNotNone(obj)

    def test_equal(self):
        obj = WavefierTriggerMessage("unit_test", Trigger(), WdfParam(), WitheningParam())
        obj2 = WavefierTriggerMessage("unit_test", Trigger(), WdfParam(), WitheningParam())
        self.assertEqual(obj, obj2)

    def test_kafka_invariance(self):

        broker = "kafka_test:9092"
        channel = "unit_test00"
        message = WavefierTriggerMessage(channel, Trigger(), WdfParam(), WitheningParam())

        producer = KafkaProducer({'bootstrap.servers': broker})
        producer.publish_message(message)

        consumer = KafkaConsumer({
            'bootstrap.servers': broker,
            'group.id': 'testGroups',
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })

        consumer.subscribe(WavefierTriggerMessage.make_template_from_channel(channel))

        msg_consumed = consumer.consume_message()

        self.assertEqual(message, msg_consumed)


