import os
from unittest import TestCase

from wavefier_trigger_handlers.core.SupplierTopicAdapter import SupplierTopicAdapter


class SuplierTopicAdapterTest(TestCase):

    def test_creation_obect_ok(self):
        try:
            obj = SupplierTopicAdapter()
        except TypeError as identifier:
            self.assertIsNotNone(identifier)

    def test_creation_obect(self):
        obj = SupplierTopicAdapter("Ciao")
        self.assertIsNotNone(obj)

    def test_get_topic(self):
        obj = SupplierTopicAdapter("Ciao")
        self.assertEqual("Ciao", obj.get_topic())
