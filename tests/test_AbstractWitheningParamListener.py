from unittest import TestCase

from wavefier_common.param.WitheningParam import WitheningParam

from wavefier_trigger_handlers.AbstractWitheningParamListener import AbstractWitheningParamListener


class tMyListner(AbstractWitheningParamListener):

    def __init__(self, owner, excepts) -> None:
        super().__init__()
        self.owner = owner
        self.excepts = excepts

    def newWitheningParamAppears(self, witheningParam: WitheningParam):
        self.owner.assertEqual(self.excepts, witheningParam)


class tNotMyListner(AbstractWitheningParamListener):

    def __init__(self, owner) -> None:
        super().__init__()
        self.owner = owner

    def newWitheningParamAppears(self, witheningParam: WitheningParam):
        self.owner.fail("it's called but it should be not called!")

class AbstractWitheningParamListenerTest(TestCase):

    def test_create(self):
        tl=AbstractWitheningParamListener()
        self.assertIsNotNone(tl)
        try:
            tl.update(self,"pippo")
            self.fail('need Exceptions')
        except :
            pass
        

    def test_newWitheningParamAppears_ok(self):
        excepted = WitheningParam()
        i=tMyListner(self,excepted)
        i.update(self,excepted)

    def test_newWitheningParamAppears_ko(self):
        i=tNotMyListner(self)
        i.update(self,"dummy")
