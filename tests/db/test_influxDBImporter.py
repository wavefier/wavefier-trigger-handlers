from random import random
from unittest import TestCase

from influxdb import InfluxDBClient
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerCollection import TriggerCollection

import tests
from wavefier_trigger_handlers.core.WavefierTriggerMessage import \
    WavefierTriggerMessage
from wavefier_trigger_handlers.db.InfluxDBImporter import InfluxDBImporter

wdfParam = WdfParam()
wthParam = WitheningParam()
hostname = tests.host_db
port = tests.port_db
channel = tests.channel
database_name = 'test_db_trigger_'+str(random())


def checkDatabase( client, to_check) -> bool:
    rs = client.query('select * from Trigger')

    if rs.error:
        return "Error on query"

    triggers = list(rs.get_points(measurement='Trigger'))

    if len(triggers) != 1:
        return "No single object as query"

    return triggers[0].get('wave_name') == to_check


class TestInfluxDBImporter(TestCase):

    def setUp(self) -> None:
        self.db = InfluxDBClient(hostname, 8086, '', '')
        self.db.create_database(database_name)
        self.db.switch_database(database_name)

    def tearDown(self) -> None:
        self.db.drop_database(database_name)

    def test_parsers(self):
        event = InfluxDBImporter.make_event(channel, Trigger(), wdfParam, wthParam);

        self.assertIsNotNone(event)

    def test_importTrigger(self):
        idb = InfluxDBImporter(hostname, port, database_name)

        trigger = Trigger(wave='TEST'+str(random()))

        self.assertTrue(idb.importTrigger(channel, trigger, wdfParam, wthParam))

        self.assertTrue(checkDatabase(self.db, trigger.getWave()))

    def test_importWavefierTriggerMessage(self):

        trigger = Trigger(wave='TEST'+str(random()))

        wtm = WavefierTriggerMessage(channel, trigger, wdfParam, wthParam)

        idb = InfluxDBImporter(hostname, port, database_name)

        self.assertTrue(idb.importWavefierTriggerMessage(wtm))

        self.assertTrue(checkDatabase(self.db, trigger.getWave()))


    def test_newTriggerAppears(self):
        trigger = Trigger(wave='TEST'+str(random()))

        wtm = WavefierTriggerMessage(channel, trigger, wdfParam, wthParam)

        idb = InfluxDBImporter(hostname, port, database_name)

        idb.newTriggerAppears(wtm)

        self.assertTrue(idb.importWavefierTriggerMessage(wtm))

        self.assertTrue(checkDatabase(self.db, trigger.getWave()))

    def test_importTriggerCollection(self):

        trigger = Trigger(wave='TEST'+str(random()))

        ic = TriggerCollection()

        ic.add(trigger)

        idb = InfluxDBImporter(hostname, port, database_name)

        idb.importTriggerCollection(ic)

        self.assertTrue(checkDatabase(self.db, trigger.getWave()))
