from unittest import TestCase


from click.testing import CliRunner
from wavefier_trigger_handlers.cli.InfluxDBImporterDaemon import influxDB_importer_daemon

class TestInfluxDB_importer_daemon(TestCase):

    def test_influxDB_importer_daemon(self):
        runner = CliRunner()
        result = runner.invoke(influxDB_importer_daemon, ['--help'])

        excepted = """Usage: influxdb-importer-daemon [OPTIONS]

Options:
  -sc, --source-channel TEXT      The source channel where the trigger is
                                  calculated
  -ih, --influxdb-host TEXT       The InfluxDB host where store the Trigger
  -ip, --influxdb-port INTEGER    he InfluxDB port where store the Trigger
  -idb, --influxdb-db-name TEXT   he InfluxDB database hame where store the
                                  Trigger
  -kbh, --kafka-broker-host TEXT  The kafka broker host where listener the
                                  Trigger
  -kbp, --kafka-broker-port INTEGER
                                  The kafka broker port where listener the
                                  Trigger
  --log-level [DEBUG|INFO|WARNING|ERROR|CRITICAL]
                                  Verbose or not verbose computation  [default:
                                  INFO]
  --help                          Show this message and exit.
"""
        self.assertEqual(result.output, excepted)
        assert result.exit_code == 0

