from unittest import TestCase
from click.testing import CliRunner

from wavefier_trigger_handlers.cli.CSV2InfluxDB import CSV2Influxdb

class TestCSV2Influxdb(TestCase):

    def test_CSV2Influxdb(self):
        runner = CliRunner()
        result = runner.invoke(CSV2Influxdb, ['--help'])
        excepted="""Usage: csv2influxdb [OPTIONS] FILENAME

Options:
  -sc, --source-channel TEXT      The source channel where the trigger is
                                  calculated  [default:
                                  V1:Hrec_hoft_16384Hz_O2Replay2]
  -ar, --ar INTEGER               The AR paramenter  [default: 3000]
  -ws, --window-size INTEGER      Windows Size  [default: 1024]
  -wo, --window-overlap INTEGER   Window Overlap  [default: 16]
  -snr, --snr INTEGER             the mininum SNR threshould  [default: 4]
  -ih, --influxdb-host TEXT       The InfluxDB host where store the Trigger
                                  [default: 192.168.178.47]
  -ip, --influxdb-port INTEGER    he InfluxDB port where store the Trigger
                                  [default: 8086]
  -idb, --influxdb-db-name TEXT   he InfluxDB database hame where store the
                                  Trigger  [default: trigger_report_db]
  --log-level [DEBUG|INFO|WARNING|ERROR|CRITICAL]
                                  Verbose or not verbose computation  [default:
                                  INFO]
  --help                          Show this message and exit.
"""

        self.assertEqual(result.output ,excepted )
        assert result.exit_code == 0

