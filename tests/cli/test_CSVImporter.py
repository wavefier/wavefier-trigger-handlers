from unittest import TestCase

from click.testing import CliRunner
from wavefier_trigger_handlers.cli.CSVImporter import CSVImporter


class TestCSVImporter(TestCase):
    def test_CSVImporter(self):
        runner = CliRunner()
        result = runner.invoke(CSVImporter, ['--help'])
        excepted = """Usage: csvimporter [OPTIONS] FILENAME

Options:
  -sc, --source-channel TEXT      The source channel where the trigger is
                                  calculated
  -ar, --ar INTEGER               The AR parameter  [default: 3000]
  -ws, --window-size INTEGER      Windows Size  [default: 1024]
  -wo, --window-overlap INTEGER   Window Overlap  [default: 16]
  -snr, --snr FLOAT               the mininum SNR threshould  [default: 4.0]
  -kbh, --kafka-broker-host TEXT  The kafka broker host where listener the
                                  Trigger  [default: 34.245.53.118]
  -kbp, --kafka-broker-port INTEGER
                                  The kafka broker port where listener the
                                  Trigger  [default: 9092]
  --log-level [DEBUG|INFO|WARNING|ERROR|CRITICAL]
                                  Verbose or not verbose computation  [default:
                                  INFO]
  --help                          Show this message and exit.
"""

        self.assertEqual(result.output, excepted)
        assert result.exit_code == 0


