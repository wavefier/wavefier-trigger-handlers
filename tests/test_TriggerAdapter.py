import os
from unittest import TestCase


import shutil

from wavefier_common.util.Path import Path
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam

from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter

from wavefier_common.kafka.Producer import KafkaProducer
from wavefier_common.kafka.Consumer import KafkaConsumer

from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.Trigger import Trigger



class TriggerAdapterTest(TestCase):

    def test_from_CSV(self):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')
        file_path = str(Path(subdir)/'test_csv')
        triggers = TriggerAdapter.fromCSV(
            file_path,
            "V1:Hrec_hoft_16384Hz",
            WitheningParam(3000),
            WdfParam(1024, 16, 4)
        )
        
        self.assertEqual(triggers.count(), 4)

        triggerIterator = triggers.iterator()

        exp0 = "2018-10-12 20:41:23.035156 UTC (GPS Time: 1223412074.0351562) SNR:4.732902094801425 SNRMax:2.1322536524135 Freq:336.0 MAXFreq:280.0 Duration:0.0 Wave:BsplineC309 NCoef:1024"
        exp1 = "2018-10-12 20:41:23.258545 UTC (GPS Time: 1223412074.258545) SNR:4.102190791407227 SNRMax:2.085527050412032 Freq:96.0 MAXFreq:68.0 Duration:0.0 Wave:BsplineC309 NCoef:1024"
        exp2 = "2019-01-12 20:13:32 UTC (GPS Time: 1231359203.0) SNR:4.732902094801425 SNRMax:2.1322536524135 Freq:336.0 MAXFreq:280.0 Duration:0.0 Wave:BsplineC309 NCoef:1024"
        exp3 = "2019-01-12 20:31:02 UTC (GPS Time: 1231360253.0) SNR:4.102190791407227 SNRMax:2.085527050412032 Freq:96.0 MAXFreq:68.0 Duration:0.0 Wave:BsplineC309 NCoef:1024"
        self.assertEqual(exp0, str(next(triggerIterator)))
        self.assertEqual(exp1, str(next(triggerIterator)))
        self.assertEqual(exp2, str(next(triggerIterator)))
        self.assertEqual(exp3, str(next(triggerIterator)))

    # def _test_from_CSV_to_Kafka_to_CSV(self):
    #     script_dir = os.path.abspath(os.path.dirname(__file__))
    #     subdir = os.path.join(script_dir, 'test_static_data')
    #     file_path = str(Path(subdir)/'test_csv')
    #
    #     triggers = TriggerAdapter.fromCSV(
    #         file_path, "V1:Hrec_hoft_16384Hz", 1223412073, WitheningParam(3000), WdfParam(1024, 16, 4)
    #     )
    #
    #
    #
    #     broker = "cumbrae.redirectme.net:9092"
    #
    #     producer = KafkaProducer({'bootstrap.servers': broker})
    #     producer.publish_message(triggers)
    #
    #     consumer = KafkaConsumer({
    #         'bootstrap.servers': broker,
    #         'group.id': 'mygroup',
    #         'default.topic.config': {
    #             'auto.offset.reset': 'smallest'
    #         }
    #     })
    #
    #     consumer.subscribe(triggers)
    #     msg_consumed = consumer.consume_message()
    #
    #
    #     self.assertTrue(isinstance(msg_consumed, TriggerCollection))
    #     self.assertEqual(msg_consumed.count(),2)
    #
    #     triggerIterator = msg_consumed.iterator()
    #
    #     exp0 = "GPS:1223412074.0351562 SNR:4.732902094801425 SNRMax:2.1322536524135 Freq:336.0 MAXFreq:280.0 Duration:0.0 Wave:BsplineC309 NCoef:1024"
    #     exp1 = "GPS:1223412074.258545 SNR:4.102190791407227 SNRMax:2.085527050412032 Freq:96.0 MAXFreq:68.0 Duration:0.0 Wave:BsplineC309 NCoef:1024"
    #     self.assertEqual(exp0, str(next(triggerIterator)))
    #     self.assertEqual(exp1, str(next(triggerIterator)))
    #
    #

#     def _test_from_BIG_CSV(self):
#         script_dir = os.path.abspath(os.path.dirname(__file__))
#         subdir = os.path.join(script_dir, 'test_static_data')
#         file_path = str(Path(subdir)/'WDFTrigger-V1Hrec_hoft_16384Hz-GPS1223412073-AR3000-Win1024-Ov16-SNR4.csv')
#         print("\n>>>>>>>LOAD<<<<<<<<")
#         triggers = TriggerAdapter.fromCSV(
#             file_path, "V1:Hrec_hoft_16384Hz", 1223412073, WitheningParam(3000), WdfParam(1024, 16, 4))
#         print("\n>>>>>>>DONE<<<<<<<<",triggers.count())


