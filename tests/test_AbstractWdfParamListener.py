from unittest import TestCase

from wavefier_common.param.WdfParam import WdfParam

from wavefier_trigger_handlers.AbstractWdfParamListener import AbstractWdfParamListener


class tMyListner(AbstractWdfParamListener):

    def __init__(self, owner, excepts) -> None:
        super().__init__()
        self.owner = owner
        self.excepts = excepts

    def newWdfParamListnerAppears(self, wdfParam: WdfParam):
        self.owner.assertEqual(self.excepts, wdfParam)


class tNotMyListner(AbstractWdfParamListener):

    def __init__(self, owner) -> None:
        super().__init__()
        self.owner = owner

    def newWdfParamAppears(self, wdfParam: WdfParam):
        self.owner.fail("it's called but it should be not called!")

class AbstractWdfParamListnerTest(TestCase):

    def test_create(self):
        tl=AbstractWdfParamListener()
        self.assertIsNotNone(tl)
        try:
            tl.update(self,"pippo")
            self.fail('need Exceptions')
        except :
            pass
        

    def test_newWdfParamAppears_ok(self):
        excepted = WdfParam('a','b')
        i=tMyListner(self,excepted)
        i.update(self,excepted)

    def test_newWdfParamAppears_ko(self):
        i=tNotMyListner(self)
        i.update(self,"dummy")
