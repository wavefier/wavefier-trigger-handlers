import logging
import os
import shutil
from unittest import TestCase

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.Trigger import Trigger

import tests
import wavefier_trigger_handlers
from wavefier_trigger_handlers.AbstractTriggerListener import AbstractTriggerListener
from wavefier_trigger_handlers.TriggerSupplier import TriggerSupplier


class MyListener(AbstractTriggerListener):

    def __init__(self, owner, sup, excepted):
        self.owner = owner
        self.sup = sup
        self.excepted = excepted

    def newTriggerAppears(self, trigger: Trigger):
        self.sup.stopObserving()
        self.owner.assertEqual(self.excepted, trigger)


class TriggerSupplierTest(TestCase):

    def test_new(self):
        sup = TriggerSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port)
        self.assertIsNotNone(sup)

    def test_start(self):
        sup = TriggerSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port,timeout=3)
        sup.startObserving()

        self.assertIsNotNone(sup)

        sup.stopObserving()

    def test_stop(self):
        sup = TriggerSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port, timeout=3)
        sup.startObserving()
        sup.stopObserving()

        self.assertIsNotNone(sup)

    def test_send_and_receive(self):
        iTrigger = Trigger('a', 'b')

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}
        producer = Producer(conf)

        message = Message(wavefier_trigger_handlers.DEFAULT_TRIGGER_TOPIC_NAME, object=iTrigger)
        responce = producer.sync_delivery(message)


        sup = TriggerSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port)
                       
        sup.addTriggerListener(MyListener(self, sup, iTrigger))
        sup.startObserving()

        sup.join(10)

        if sup.is_alive():
            self.fail("Timeout on Tread Return")
        sup.stopObserving()

