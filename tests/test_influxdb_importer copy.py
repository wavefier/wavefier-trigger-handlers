import os
from unittest import TestCase
from wavefier_trigger_handlers.db.InfluxDBImporter import InfluxDBImporter
from wavefier_common.util.Path import Path
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam

from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter
import shutil


import logging
logging.basicConfig(level=logging.DEBUG)



class InfluxDBImporterWdfParamTest(TestCase):

    def test_creation(self):
        host_db = "34.245.53.118"
        port_db = 8086
        db_name = "trigger_report_db"

        importer=InfluxDBImporter(host_db,port_db,db_name)

    def test_importTrigger(self):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')
        file_path = str(Path(subdir)/'bb.csv')

        triggers = TriggerAdapter.fromCSV(
            file_path,
            "V1:Hrec_hoft_16384Hz",
            WitheningParam(4000),
            WdfParam(1024, 16, 2.0)
        )

        host_db = "192.168.178.47"
        port_db = 8086
        db_name = "trigger_report_db"

        importer=InfluxDBImporter(host_db,port_db,db_name)

        importer.importTriggers(triggers)

