import logging
import os
import random
from unittest import TestCase

import coloredlogs
from wavefier_common.kafka.avro.Consumer import Consumer
from wavefier_common.kafka.avro.Message import Message
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerCollection import TriggerCollection

import tests
import wavefier_trigger_handlers
from wavefier_trigger_handlers.core.WavefierTriggerMessage import \
    WavefierTriggerMessage
from wavefier_trigger_handlers.TriggerDistributor import TriggerDistributor


class TestAvroTriggerDistributor(TestCase):

    def setUp(self):
        coloredlogs.install(level="DEBUG")
        self.channel = tests.channel
        self.wdfParam = WdfParam()
        self.wthParam = WitheningParam()
        self.trigger = Trigger(WDFParam_id=self.wdfParam.uuid,
                               WTHParam_id=self.wthParam.uuid, channel=self.channel)
        self.logger = logging.getLogger("TEST LOG")
        

    def test_deploy_wdfParam(self):
        
        self.logger.info("__________  Start Test")
        tb = TriggerDistributor(
            tests.broker_host, tests.broker_host, 
            tests.registry_host, tests.registry_port
        )

        self.logger.info("__________  DELIVERY Test")
        tb.deliveryWdfParam(self.wdfParam)
        
        self.logger.info("__________  CECK Test")
        self.checkWDFMessageOnKafka()

    def test_deploy_wthParam(self):
        tb = TriggerDistributor(
            tests.broker_host, tests.broker_host, 
            tests.registry_host, tests.registry_port)
        tb.deliveryWitheningParam(self.wthParam)
        self.checkWthMessageOnKafka()

    def test_deploy(self):
        tb = TriggerDistributor(
            tests.broker_host, tests.broker_host, tests.registry_host, tests.registry_port)
        tb.deploy(self.channel, self.wthParam, self.wdfParam, self.trigger)
        self.checkMessageOnKafka()

    def test_deployRow(self):
        tb = TriggerDistributor(
            tests.broker_host, tests.broker_host, tests.registry_host, tests.registry_port)
        tb.deployRow(self.channel, self.wthParam.AR, self.wdfParam.window,
                     self.wdfParam.overlap, self.wdfParam.threshold, self.trigger)
        self.checkMessageOnKafka()

    def test_deployAll(self):
        tb = TriggerDistributor(
            tests.broker_host, tests.broker_host, 
            tests.registry_host, tests.registry_port
        )
        tc = TriggerCollection(self.channel, self.wthParam, self.wdfParam)
        tb.deployAll(tc)
        self.checkMessageOnKafka()


    def checkTriggerMessageOnKafka(self):
        consumerTrigger = Consumer({
            'bootstrap.servers': tests.broker_host + ":" + str(tests.broker_port),
            'group.id': 'testGroups' + str(random.random()),
            'schema.registry.url': tests.registry_host + ":" + str(tests.registry_port),
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })

        consumerTrigger.subscribe(Message(
            wavefier_trigger_handlers.DEFAULT_TRIGGER_TOPIC_NAME, Trigger("dummy", "dummy")))
        msg_consumed = consumerTrigger.consume_message(5)
        self.assertEqual(self.trigger, msg_consumed)
    
    def checkWthMessageOnKafka(self):

        consumerWth = Consumer({
            'bootstrap.servers': tests.broker_host + ":" + str(tests.broker_port),
            'group.id': 'testGroups' + str(random.random()),
            'schema.registry.url': tests.registry_host + ":" + str(tests.registry_port),
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })

        consumerWth.subscribe(
            Message(wavefier_trigger_handlers.DEFAULT_WTHPARAM_TOPIC_NAME, WitheningParam()))
        msg_consumed = consumerWth.consume_message(5)
        self.assertEqual(self.wthParam, msg_consumed)

    def checkWDFMessageOnKafka(self):

        self.logger.info("__________  CECK CREATE CONSUMER")

        consumerWdf = Consumer({
            'bootstrap.servers': tests.broker_host + ":" + str(tests.broker_port),
            'group.id': 'testGroups' + str(random.random()),
            'schema.registry.url': tests.registry_host + ":" + str(tests.registry_port),
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })

        self.logger.info("__________  CECK SUBSCRIVE MESSAGE")

        consumerWdf.subscribe(Message(
            wavefier_trigger_handlers.DEFAULT_WDFPARAM_TOPIC_NAME, WdfParam()))

        self.logger.info("__________  CECK Consume MESSAGE")

        msg_consumed = consumerWdf.consume_message(5)
        
        self.logger.info("__________  CECK CheckRisponce")

        self.assertEqual(self.wdfParam, msg_consumed)
