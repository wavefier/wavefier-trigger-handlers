from unittest import TestCase

from wavefier_common.Trigger import Trigger

from wavefier_trigger_handlers.AbstractTriggerListener import AbstractTriggerListener


class tMyListner(AbstractTriggerListener):

    def __init__(self, owner, excepts) -> None:
        super().__init__()
        self.owner = owner
        self.excepts = excepts

    def newTriggerAppears(self, trigger: Trigger):
        self.owner.assertEqual(self.excepts, trigger)


class tNotMyListner(AbstractTriggerListener):

    def __init__(self, owner) -> None:
        super().__init__()
        self.owner = owner

    def newTriggerAppears(self, trigger: Trigger):
        self.owner.fail("it's called but it should be not called!")

class AbastractTriggerListenerTest(TestCase):

    def test_create(self):
        tl=AbstractTriggerListener()
        self.assertIsNotNone(tl)
        try:
            tl.update(self,"pippo")
            self.fail('need Exceptions')
        except :
            pass
        

    def test_newTriggerAppears_ok(self):
        excepted = Trigger('a','b')
        i=tMyListner(self,excepted)
        i.update(self,excepted)

    def test_newTriggerAppears_ko(self):
        i=tNotMyListner(self)
        i.update(self,"dummy")



