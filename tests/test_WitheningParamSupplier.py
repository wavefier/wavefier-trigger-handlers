import logging
import os
import shutil
from unittest import TestCase

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.param.WitheningParam import WitheningParam

import tests
import wavefier_trigger_handlers
from wavefier_trigger_handlers.AbstractWitheningParamListener import AbstractWitheningParamListener
from wavefier_trigger_handlers.WitheningParamSupplier import WitheningParamSupplier


class MyListener(AbstractWitheningParamListener):

    def __init__(self, owner, sup, excepted):
        self.owner = owner
        self.sup = sup
        self.excepted = excepted

    def newWitheningParamAppears(self, witheningParam: WitheningParam):
        self.sup.stopObserving()
        self.owner.assertEqual(self.excepted, witheningParam)


class WitheningParamSupplierTest(TestCase):

    def test_new(self):
        sup = WitheningParamSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port)
        self.assertIsNotNone(sup)

    def test_start(self):
        sup = WitheningParamSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port,timeout=3)
        sup.startObserving()

        self.assertIsNotNone(sup)

        sup.stopObserving()

    def test_stop(self):
        sup = WitheningParamSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port, timeout=3)
        sup.startObserving()
        sup.stopObserving()

        self.assertIsNotNone(sup)

    def test_send_and_receive(self):
        iWitheningParam = WitheningParam()

        conf = {'bootstrap.servers': tests.kafka_broker,
                'schema.registry.url': tests.kafka_schema_registry}
        producer = Producer(conf)

        message = Message(wavefier_trigger_handlers.DEFAULT_WDFPARAM_TOPIC_NAME, object=iWitheningParam)
        responce = producer.sync_delivery(message)


        sup = WitheningParamSupplier(tests.broker_host, tests.broker_port,
                       tests.registry_host, tests.registry_port)
                       
        sup.addWitheningParamListener(MyListener(self, sup, iWitheningParam))
        sup.startObserving()

        sup.join(10)

        if sup.is_alive():
            self.fail("Timeout on Tread Return")
        sup.stopObserving()

