import os
from unittest import TestCase

import wavefier_trigger_handlers



class VersionTest(TestCase):

    def test_version(self):
        self.assertIsNotNone(wavefier_trigger_handlers.__author__)
        self.assertIsNotNone(wavefier_trigger_handlers.__version__)
