import logging
import threading
import random

from wavefier_trigger_handlers import AbstractTriggerListener
from wavefier_trigger_handlers.core.WavefierSupervisedMLMessage import WavefierSupervisedMLMessage
from wavefier_common.kafka.Consumer import KafkaConsumer
from wavefier_common.util.Observable import Observable



class TriggerSupervisedMLSupplier(threading.Thread):
    """This class have the responsibility do provide trigger from Kafka System.

    It use the  Observer pattern. This object, called the subject, maintains a list of its dependents, called observers or Listener,
    and notifies them automatically of any trigger appears on Kafka System.

    This Class is a extension of :class:`threading.Thread`

    Args:
          **broker_host** (str) the Kafka Broker Host

          **broker_port** (int) the Kafka Broker Listen Port

          **logger** Specify a custom logger

    """

    def __init__(self, broker_host: str, broker_port: int = 9092, logger=None) -> None:

        super().__init__(name="TriggerSupervisedMLSupplier_Worker")

        self.logger = logger or logging.getLogger(__name__)

        self.__isStarted = False

        self.__consumer = KafkaConsumer({
            'bootstrap.servers': broker_host + ":" + str(broker_port),
            'group.id': 'TriggerObserverGroup'  + str(random.random()),
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })
        self._observable = Observable()
        self.__isStarted = False

        self.logger.debug("Class Bootstrap")

    def run(self):
        """"""
        self.logger.info('Worker Running')
        while self.__isStarted:
            self.logger.info("New cicle of while")
            try:
                msg_consumed = self.__consumer.consume_message()
                self.logger.info("message consumed " + str(msg_consumed))
                self._observable.setChanged()
                self.logger.info("Is changed something")
                self._observable.notifyObservers(msg_consumed)
                self.logger.info("After notify")

            except Exception as ex:
                self.logger.exception("Problem on Kafka consume Message: ")

            self.logger.debug("end of while While")

    def startObserving(self):
        """Start to Observing trigger in a specific channel

        :param channel: The channel to observing

        """
        self.logger.info('Starting the observing')
        self.__consumer.subscribe(WavefierSupervisedMLMessage.make_template())

        self.__isStarted = True
        self.start()

    def stopObserving(self):
        """Stop Observing Kafka

        """
        self.logger.info('Stop the observing')

        self.__isStarted = False

    def addTriggerListener(self, listener: AbstractTriggerListener):
        """Add new Trigger Listener on Supplier

        :param listener: the observer to call in case of Trigger

        """
        self.logger.info('New Trigger Listener Added')

        self._observable.addObserver(listener)

    def delTriggerListener(self, listener: AbstractTriggerListener):
        """Remove the Trigger Listener from the Supplier

        :param listener: the observer to call in case of Trigger

        """
        self.logger.info('New Trigger Listener deleted')

        self._observable.deleteObserver(listener)
