import logging

from abc import abstractmethod

from wavefier_common.util import Observable
from wavefier_common.param.WdfParam import WdfParam


class AbstractWdfParamListener(object):
    """Abstract class that represent the Observer of WdfParam.

    Args:
        **Logger**  A different logger

    """

    def __init__(self, logger=None) -> None:
        self.logger = logger or logging.getLogger(__name__)

    def update(self, observable: Observable, arg):
        """"""
        #super().update(observable, arg)

        if isinstance(arg, WdfParam):
            self.newWdfParamAppears(arg)
        else:
            self.logger.warning("Received an unrecognized message, ignored!")


    @abstractmethod
    def newWdfParamAppears(self, wdfParam: WdfParam):
        """Function executed whenever a trigger is present

        :param trigger: WavefierWdfParamMessage  aka the trigger received

        """
        pass
