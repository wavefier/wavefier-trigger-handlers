import logging

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.param.WdfParam import WdfParam

import wavefier_trigger_handlers
from wavefier_trigger_handlers.AbstractWdfParamListener import \
    AbstractWdfParamListener
from wavefier_trigger_handlers.core.Supplier import Supplier


class WdfParamSupplier(Supplier):
    """This class have the responsibility do provide wdfParam from Kafka System.

    It use the  Observer pattern. This object, called the subject, maintains a list of its dependents, called observers or Listener,
    and notifies them automatically of any wdfParam appears on Kafka System.

    This Class is a extension of :class:`threading.Thread`

    Args:
          **broker_host** (str) the Kafka Broker Host

          **broker_port** (int) the Kafka Broker Listen Port

          **logger** Specify a custom logger

    """

    def __init__(self, broker_host: str, broker_port: int = 9092,
                 registry_host: str = '', registry_port: int = 8081, timeout=30, logger=None) -> None:

        super().__init__(broker_host, broker_port,
                         registry_host, registry_port,
                         'WdfParamSupplier', timeout, logger)

        self.logger = logger or logging.getLogger(__name__)
    
    def startObserving(self):
        # TODO REMOTE THIS WORKARROUND!!!
        iTemplateMessage = Message(wavefier_trigger_handlers.DEFAULT_WDFPARAM_TOPIC_NAME, WdfParam())
        return super().startObserving(iTemplateMessage)

    def addWdfParamListener(self, listener: AbstractWdfParamListener):
        """Add new WdfParam Listener on Supplier

        :param listener: the observer to call in case of WdfParam

        """
        if isinstance(listener, AbstractWdfParamListener):
            self.logger.debug('New WdfParam Listener Added')
            self._observable.addObserver(listener)
        else:
            self.logger.warning("Received an unrecognized listner, ignored!")

    def delWdfParamListener(self, listener: AbstractWdfParamListener):
        """Remove the WdfParam Listener from the Supplier

        :param listener: the observer to call in case of WdfParam

        """

        if isinstance(listener, AbstractWdfParamListener):
            self.logger.debug('New WdfParam Listener deleted')
            self._observable.deleteObserver(listener)
        else:
            self.logger.warning("Received an unrecognized listner, ignored!")
