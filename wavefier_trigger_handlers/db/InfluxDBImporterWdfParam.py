import logging
import sys
from time import sleep

import wavefier_common
from influxdb import InfluxDBClient
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.util.Time import Time as TimeConverter
from wavefier_common.kafka.avro.Message import Message

from wavefier_trigger_handlers.AbstractWdfParamListener import \
    AbstractWdfParamListener


class InfluxDBImporterWdfParam(AbstractWdfParamListener):
    """This class is responsible for inserting a trigger object into the InfluxDB database

    A listener that implement the DB importer

    Args:
        **hostname** the hostname where is InfluxDB
        **port** the port where is InfluxDB
        **database_name** The database Name
        **logger** A custom Logger
    """

    def __init__(self, hostname: str, port: int, database_name: str, retry: int = 5) -> None:
     

        self._connectionHost = hostname
        self._connectionPort = port
        self._connectionDatabase = database_name
        self.retry = retry

    @staticmethod
    def make_event(wdfParam: WdfParam):
        dic=Message.dic_from_object(wdfParam)
        return [{
            "measurement": 'WdfParam',
            "tags": dic,
            "fields": dic
        }]

    def _import(self, wdfParam: WdfParam) -> bool:
        out = True
        try:
            client = InfluxDBClient(
                self._connectionHost, self._connectionPort, '', '', self._connectionDatabase)
            json_body = self.make_event(wdfParam)
            client.write_points(json_body)
            client.close()
            logging.info(">>>>>>>>>>>>>>> Message SENT ON DB")
        except:
            out = False
            logging.info("Error on save TriggerSupervisedResults on Database")

        return out

    def newWdfParamAppears(self, wdfParam: WdfParam) -> bool:

        done = False
        cretry = 0
        while not done and cretry <= self.retry:
            done = self._import(wdfParam)
            if not done:
                cretry = cretry + 1
                sleep(0.1)
                logging.info("Try one more time...")

        return done
