import datetime
import json
import sys
from time import sleep

import wavefier_common
from influxdb import InfluxDBClient
from wavefier_common.kafka.avro.Message import Message
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.util.Time import Time as TimeConverter

from wavefier_trigger_handlers.AbstractTriggerListener import \
    AbstractTriggerListener


class InfluxDBImporter(AbstractTriggerListener):
    """This class is responsible for inserting a trigger object into the InfluxDB database

    A listener that implement the DB importer

    Args:
        **hostname** the hostname where is InfluxDB
        **port** the port where is InfluxDB
        **database_name** The database Name
        **logger** A custom Logger
    """

    def __init__(self, hostname: str, port: int, database_name: str,  retry: int = 5, logger=None) -> None:
        super().__init__(logger)
        self.logger.debug("Received: ", self.__dict__)
        self._connectionHost = hostname
        self._connectionPort = port
        self._connectionDatabase = database_name
        self.retry = retry

    @staticmethod
    def make_event(trigger: Trigger):
        time_report = datetime.datetime.now().replace(microsecond=0).isoformat()
        time_hreadable = TimeConverter.gps2isoformat(trigger.getGPS())

        dic = Message.dic_from_object(trigger)
        dic.pop('rw', None)
        dic.pop('wt', None)
        dic.update({"time_report": time_report})
        dic.update({"time_hreadable": time_hreadable})
        
        return [{
            "measurement": "Trigger",
            "time": time_hreadable,
            "tags": dic,
            "fields": dic
        }]

    def _importTrigger(self, itrigger: Trigger) -> bool:
        """ Import a single trigger on DB

        :param channel:  The channel involved

        :param itrigger: The trigger

        :param iwdf_param:  The Wdf Param

        :param iwithening_param: The Withening Param

        :return: The result of import
        """
        out = True
        try:
            self.logger.debug('A - Open InfluxDB connection')
            client = InfluxDBClient(
                self._connectionHost, self._connectionPort, '', '', self._connectionDatabase)
            self.logger.debug('B - Generate Message... ')
            json_body = self.make_event(itrigger)
            self.logger.debug('C - Insert message: ' + str(json_body))
            client.write_points(json_body,time_precision='ms',protocol='json')
            self.logger.debug('D - Close InfluxDB')
            client.close()
        except:
            out = False
            self.logger.exception("Error on save Trigger on Database")

        return out

    def importTrigger(self, itrigger: Trigger) -> bool:
        """ Import a single trigger on DB

        :param channel:  The channel involved

        :param itrigger: The trigger

        :param iwdf_param:  The Wdf Param

        :param iwithening_param: The Withening Param

        :param retry: the number of retry default 5 time

        :return: The result of import
        """
        done = False
        cretry = 0
        self.logger.debug("Try to insert Trigger on InfluxDB...")
        while not done and cretry <= self.retry:
            done = self._importTrigger(itrigger)
            if not done:
                cretry = cretry + 1
                sleep(0.1)
                self.logger.warning("Try one more time...")

        return done

    def newTriggerAppears(self, trigger: Trigger) -> bool:
        """Overload of Listener Function executed whenever a trigger is present

        :param triggerWavefier: WavefierTriggerMessage  aka the trigger received

        :param retry: the number of retry default 5 time

        :return: The result of import
        """
        return self.importTrigger(trigger)
