import logging

from abc import abstractmethod

from wavefier_common.util import Observable
from wavefier_common.param.WitheningParam import WitheningParam


class AbstractWitheningParamListener(object):
    """Abstract class that represent the Observer of WitheningParam.

    Args:
        **Logger**  A different logger

    """

    def __init__(self, logger=None) -> None:
        self.logger = logger or logging.getLogger(__name__)

    def update(self, observable: Observable, arg):
        """"""
        #super().update(observable, arg)

        if isinstance(arg, WitheningParam):
            self.newWitheningParamAppears(arg)
        else:
            self.logger.warning("Received an unrecognized message, ignored!")


    @abstractmethod
    def newWitheningParamAppears(self, witheningParam: WitheningParam):
        """Function executed whenever a trigger is present

        :param trigger: WavefierWitheningParamMessage  aka the trigger received

        """
        pass
