from wavefier_common.kafka.avro.Message import Message


class SupplierTopicAdapter(Message):

    def __init__(self, topic: str):
        if topic is None:
            raise ValueError("Inappropriate argument value topic (of correct type)")
        self.topic = topic
