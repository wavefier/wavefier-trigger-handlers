from wavefier_common.TriggerUnsupervisedResults import TriggerUnsupervisedResults
from wavefier_common.kafka.Message import KafkaMessage


class WavefierUnsupervisedMLMessage(KafkaMessage):

    def __init__(self, ml: TriggerUnsupervisedResults):
        """
        """
        super(WavefierUnsupervisedMLMessage, self).__init__(WavefierUnsupervisedMLMessage.chooseBestTopicName())
        self.ml = ml

    @staticmethod
    def chooseBestTopicName():
        return "unsupervised_ml_trigger"

    @staticmethod
    def make_template():
        """
        """
        return WavefierUnsupervisedMLMessage(TriggerUnsupervisedResults())
    
    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, WavefierUnsupervisedMLMessage):
            return self.ml == other.ml
        return False

