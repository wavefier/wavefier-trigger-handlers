from wavefier_common.Trigger import Trigger
from wavefier_common.kafka.Message import KafkaMessage
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam


class WavefierTriggerMessage(KafkaMessage):

    def __init__(self, channel: str, itrigger: Trigger, iwdf_param: WdfParam, iwithening_param: WitheningParam):
        """
        Create a message for wdf cluster
        :param channel:
        :param itrigger:
        :param iwdf_param:
        :param iwithening_param:
        """
        super(WavefierTriggerMessage, self).__init__(WavefierTriggerMessage.chooseBestTopicName(channel))
        self.channel = channel
        self.trigger = itrigger
        self.wdf_param = iwdf_param
        self.withening_param = iwithening_param

    @staticmethod
    def chooseBestTopicName(channel: str):
        #return "Trigger00_" + channel[3:]
        return "Trigger"

    @staticmethod
    def make_template_from_channel(channel: str):
        """
        Make a template of object
        :param channel: 
        :return: 
        """
        a = WdfParam()
        b = WitheningParam()
        return WavefierTriggerMessage(channel, Trigger(a.uuid, b.uuid), a, b)

    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, WavefierTriggerMessage):
            return self.channel == other.channel \
                   and self.trigger == other.trigger \
                   and self.wdf_param == other.wdf_param \
                   and self.withening_param == other.withening_param
        return False
