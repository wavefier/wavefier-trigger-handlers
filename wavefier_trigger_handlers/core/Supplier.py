import logging
import random
import threading

from wavefier_common.kafka.avro.Consumer import Consumer
from wavefier_common.kafka.avro.Message import Message
from wavefier_common.kafka.avro.TimeOutException import TimeOutException
from wavefier_common.util.Observable import Observable

from wavefier_trigger_handlers import AbstractTriggerListener
from wavefier_trigger_handlers.core.SupplierTopicAdapter import \
    SupplierTopicAdapter
from wavefier_trigger_handlers.core.WavefierTriggerMessage import \
    WavefierTriggerMessage


class Supplier(threading.Thread):
    """This class have the responsibility do provide trigger from Kafka System.

    It use the  Observer pattern. This object, called the subject, maintains a list of its dependents, called observers or Listener,
    and notifies them automatically of any trigger appears on Kafka System.

    This Class is a extension of :class:`threading.Thread`

    Args:
          **broker_host** (str) the Kafka Broker Host

          **broker_port** (int) the Kafka Broker Listen Port

          **logger** Specify a custom logger

    """

    def __init__(self,  broker_host: str, broker_port: int = 9092, 
                        registry_host: str = '', registry_port: int = 8081,     
                        name_prefix: str = 'Generic_Supplier', timeout: int = None, 
                        logger=None) -> None:

        super().__init__(name=name_prefix+"_Worker")

        self.logger = logger or logging.getLogger(__name__)

        self.__isStarted = False

        self.__consumer = Consumer({
            'bootstrap.servers': broker_host + ":" + str(broker_port),
            'group.id': name_prefix + str(random.random()),
            'schema.registry.url': registry_host + ":" + str(registry_port),
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })
        self._observable = Observable()
        self.__isStarted = False
        self.__timeout = timeout

        self.logger.debug("Class Bootstrap")

    def run(self):
        """"""
        self.logger.debug('Worker Running')
        while self.__isStarted:
            self.logger.debug("New cicle of while")
            try:
                msg_consumed = self.__consumer.consume_message(self.__timeout)
                self.logger.debug("message consumed " + str(msg_consumed))
                self._observable.setChanged()
                self.logger.debug("Is changed something")
                self._observable.notifyObservers(msg_consumed)
                self.logger.debug("After notify")
            except TimeOutException as timeout:
                self.logger.debug(timeout)
            except Exception as ex:
                self.logger.exception("Problem on Kafka consume Message: ")

            self.logger.debug("end of while While")

    def startObserving(self, message: Message):
        """Start to Observing trigger in a specific channel

        :param channel: The channel to observing

        """
        self.logger.debug('Starting the observing')
        # TODO: should change the signature of subscrive in order to remove this work arround
        self.__consumer.subscribe(message)

        self.__isStarted = True
        self.start()

    def stopObserving(self):
        """Stop Observing Kafka

        """
        self.logger.debug('Stop the observing')

        self.__isStarted = False
