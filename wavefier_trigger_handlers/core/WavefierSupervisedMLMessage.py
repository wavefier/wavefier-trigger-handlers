from wavefier_common.TriggerSupervisedResults import TriggerSupervisedResults
from wavefier_common.kafka.Message import KafkaMessage


class WavefierSupervisedMLMessage(KafkaMessage):

    def __init__(self, ml: TriggerSupervisedResults):
        """
        """
        super(WavefierSupervisedMLMessage, self).__init__(WavefierSupervisedMLMessage.chooseBestTopicName())
        self.ml = ml

    @staticmethod
    def chooseBestTopicName():
        return "supervised_ml_trigger"

    @staticmethod
    def make_template():
        """
        """
        return WavefierSupervisedMLMessage(TriggerSupervisedResults())
    
    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, WavefierSupervisedMLMessage):
            return self.ml == other.ml
        return False

