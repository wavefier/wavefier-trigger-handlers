"""Python client implementations of  wavefier trigger"""
__author__ = 'world'
__version__ = '2.0.0'

DEFAULT_TRIGGER_TOPIC_NAME="Trigger"
DEFAULT_WDFPARAM_TOPIC_NAME="Trigger_WDF"
DEFAULT_WTHPARAM_TOPIC_NAME="Trigger_WTH"