import logging

from abc import abstractmethod

from wavefier_common.util import Observable
from wavefier_common.Trigger import Trigger


class AbstractTriggerListener(object):
    """Abstract class that represent the Observer of Trigger.

    Args:
        **Logger**  A different logger

    """

    def __init__(self, logger=None) -> None:
        self.logger = logger or logging.getLogger(__name__)

    def update(self, observable: Observable, arg):
        """"""
        #super().update(observable, arg)

        if isinstance(arg, Trigger):
            self.newTriggerAppears(arg)
        else:
            self.logger.warning("Received an unrecognized message, ignored!")


    @abstractmethod
    def newTriggerAppears(self, trigger: Trigger):
        """Function executed whenever a trigger is present

        :param trigger: WavefierTriggerMessage  aka the trigger received

        """
        pass
