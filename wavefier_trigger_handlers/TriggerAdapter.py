import csv as csv
import array as arr
import logging

from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam


class TriggerAdapter(object):
    """ Utility class usable to create/manage Trigger or TriggerCollection
    """

    @staticmethod
    def fromCSV(filename: str, channel: str, wthP: WitheningParam, wdfP: WdfParam) -> TriggerCollection:
        """Generate Trigger collection from CSV file

        Args:
            :param filename: Fullpath of CSV file
            :param channel: Channel involed
            :param wthP: The Paramenter used on Withening
            :param wdfP: The Paramenter used on WDF 

            :return: TriggerCollection -- The rapresentaion of trigger

        """

        output = TriggerCollection(channel, wthP, wdfP)

        file = open(filename)
        logging.info("Load File...")
        all_dictionaries = csv.DictReader(file)
        logging.info("Fetch data:")
        for row_dictionary in all_dictionaries:
            trigger = Trigger(wdfP.uuid, wthP.uuid, row_dictionary['gps'], row_dictionary['snr'],
                              row_dictionary['snrMax'],
                              row_dictionary['freq'], row_dictionary['freqMax'], row_dictionary['duration'],
                              row_dictionary['wave'], output.getWDFParam('window'))

            if "LABEL" in row_dictionary:
                trigger.label = row_dictionary['LABEL']
            else:
                trigger.label = '-'

            for num in range(0, output.getWDFParam('window')):
                trigger.setWT(num, row_dictionary.get('wt' + str(num), 0.0))
                trigger.setRW(num, row_dictionary.get('rw' + str(num), 0.0))

            output.add(trigger)

        file.close()

        logging.info("Loaded %s triggers" % output.count())
        return output
