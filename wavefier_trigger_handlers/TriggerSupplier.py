import logging

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.Trigger import Trigger


from wavefier_trigger_handlers.core.Supplier import Supplier
from wavefier_trigger_handlers.AbstractTriggerListener import AbstractTriggerListener
import wavefier_trigger_handlers

class TriggerSupplier(Supplier):
    """This class have the responsibility do provide trigger from Kafka System.

    It use the  Observer pattern. This object, called the subject, maintains a list of its dependents, called observers or Listener,
    and notifies them automatically of any trigger appears on Kafka System.

    This Class is a extension of :class:`threading.Thread`

    Args:
          **broker_host** (str) the Kafka Broker Host

          **broker_port** (int) the Kafka Broker Listen Port

          **logger** Specify a custom logger

    """

    def __init__(self, broker_host: str, broker_port: int = 9092,
                 registry_host: str = '', registry_port: int = 8081, timeout=30, logger=None) -> None:

        super().__init__(broker_host, broker_port,
                         registry_host, registry_port,
                         'TriggerSupplier', timeout, logger)

        self.logger = logger or logging.getLogger(__name__)
    
    def startObserving(self):
        # TODO REMOTE THIS WORKARROUND!!!
        iTemplateMessage = Message(wavefier_trigger_handlers.DEFAULT_TRIGGER_TOPIC_NAME, Trigger('dummy','dummy'))
        return super().startObserving(iTemplateMessage)

    def addTriggerListener(self, listener: AbstractTriggerListener):
        """Add new Trigger Listener on Supplier

        :param listener: the observer to call in case of Trigger

        """
        if isinstance(listener, AbstractTriggerListener):
            self.logger.debug('New Trigger Listener Added')
            self._observable.addObserver(listener)
        else:
            self.logger.warning("Received an unrecognized listner, ignored!")

    def delTriggerListener(self, listener: AbstractTriggerListener):
        """Remove the Trigger Listener from the Supplier

        :param listener: the observer to call in case of Trigger

        """

        if isinstance(listener, AbstractTriggerListener):
            self.logger.debug('New Trigger Listener deleted')
            self._observable.deleteObserver(listener)
        else:
            self.logger.warning("Received an unrecognized listner, ignored!")
