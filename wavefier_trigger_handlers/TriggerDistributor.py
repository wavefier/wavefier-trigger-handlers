import logging

from wavefier_common.kafka.avro.Message import Message
from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.util.Observer import Observer

import wavefier_trigger_handlers
from wavefier_trigger_handlers.core.WavefierTriggerMessage import \
    WavefierTriggerMessage


class TriggerDistributor(Observer):
    """The TriggerDistributor provide a solution for the deploy of Trigger in Kafka System.

     It follow the Distributor Pattern and consists in a deployment of entity called  distributor and perform the acting as a proxy on Kafka system.
     The  distributor  is  connected  to  broker  where  the  kafka systems management solution resides and over LAN links to agents residing  in remote systems.

     This interface can be used to notify a new trigger to all other actors in the pipeline.

     Args:
        **broker_host** Kafka Broker Host

        **broker_port**  Kafka Broker Port

        **logging** Use a special Logging
    """

    def __init__(self,
                 broker_host: str = '', broker_port: int = 9092,
                 registry_host: str = '', registry_port: int = 8081, logger=None):

        self.logger = logger or logging.getLogger(__name__)

        broker = broker_host + ":" + str(broker_port)
        registry = registry_host + ":" + str(registry_port)

        self._kafkaProducer = Producer({
            'bootstrap.servers': broker,
            'schema.registry.url': registry
        })


    def deliveryTrigger(self, channel: str, trigger: Trigger):
        if trigger.getChannel() == channel:

            messageTrigger = Message(
                wavefier_trigger_handlers.DEFAULT_TRIGGER_TOPIC_NAME, object=trigger)
            self.logger.debug("Send: " + str(messageTrigger))
            return self._kafkaProducer.sync_delivery(messageTrigger)

        else:
            self.logger.warning(
                "Send Aborted! trigger channel and channel is different! ")
            return False

    def deliveryWitheningParam(self, wthParam: WitheningParam):

        if wthParam is not None:
            messageWP = Message(
                wavefier_trigger_handlers.DEFAULT_WTHPARAM_TOPIC_NAME, object=wthParam)
            self.logger.debug("Send: " + str(messageWP))
            return self._kafkaProducer.sync_delivery(messageWP)
        else:
            self.logger.warning("Send Aborted! object is None ")
            return False

    def deliveryWdfParam(self, wdfParam: WdfParam):
        if wdfParam is not None:

            messageWDFP = Message(
                wavefier_trigger_handlers.DEFAULT_WDFPARAM_TOPIC_NAME,
                object=wdfParam
            )

            self.logger.debug("Sent WDF Obect: " + str(messageWDFP))
            return self._kafkaProducer.sync_delivery(messageWDFP)
        else:
            self.logger.warning("Send Aborted! object is None ")
            return False

    def deploy(self, channel: str, wthParam: WitheningParam, wdfParam: WdfParam, trigger: Trigger) -> bool:
        """Provide a new Trigger.

        :param channel: (str) - The channel where the trigger appears
        :param wthParam: (WthParam) - The parameter of Whitening
        :param wdfParam: (WdfParam) - The parameter of WDF
        :param trigger: (Trigger) - The trigger to distribute

        :return: Boolean
        """
        return self.deliveryTrigger(channel, trigger) and self.deliveryWdfParam(wdfParam) and self.deliveryWitheningParam(wthParam)

    def deployRow(self, channel: str, ar: int, windowsize: int, windowoverlap: int, snr: int, trigger: Trigger) -> bool:
        """Provide a new Trigger.

        :param channel:  (str) - The channel where the trigger appears
        :param ar: (WthParam) - The parameter of Whitening
        :param windowsize: (WdfParam) - The parameter of WDF
        :param windowoverlap: (WdfParam) - The parameter of WDF
        :param snr: (WdfParam) - The parameter of WDF
        :param trigger: (Trigger) - The trigger to distribute

        :return: Boolean

        """
        self.logger.debug("Pass Row Trigger to deploy function... ")
        return self.deploy(channel, WitheningParam(ar), WdfParam(windowsize, windowoverlap, snr), trigger)

    def deployAll(self, triggerCollection: TriggerCollection) -> bool:
        """Provide a new Trigger via TriggerCollector.

        :param triggerCollection: - a Collection of trigger

        :return: Boolean

        """

        self.logger.debug(
            "Iterate TriggersCollection and pass Trigger to deploy function... ")
        for trigger in triggerCollection:
            self.deploy(triggerCollection.getChannel(), trigger, triggerCollection.getWDFObject(),
                        triggerCollection.getWTHObject())

        return True
