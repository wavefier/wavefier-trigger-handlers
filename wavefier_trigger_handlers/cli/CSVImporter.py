import coloredlogs
import click
from wavefier_common.kafka.Producer import KafkaProducer
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam
import os

from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter


@click.command()
@click.argument('filename', type=click.Path(exists=True))
@click.option('--source-channel', '-sc', default=os.environ["CHANNEL"],
              help="The source channel where the trigger is calculated")
@click.option('--ar', '-ar', default=3000, show_default=True,
              help="The AR parameter")
@click.option('--window-size', '-ws', default=1024, show_default=True,
              help="Windows Size")
@click.option('--window-overlap', '-wo', default=16, show_default=True,
              help="Window Overlap")
@click.option('--snr', '-snr', default=4.0, show_default=True,
              help="the mininum SNR threshould")
@click.option('--kafka-broker-host', '-kbh', default='34.245.53.118', show_default=True,
              help="The kafka broker host where listener the Trigger")
@click.option('--kafka-broker-port', '-kbp', default=9092, show_default=True,
              help="The kafka broker port where listener the Trigger")
@click.option('--log-level', default='INFO', show_default=True,
              type=click.Choice(['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']),
              help='Verbose or not verbose computation')
def CSVImporter(filename, source_channel, ar, window_size, window_overlap, snr, kafka_broker_host, kafka_broker_port,
                log_level):
    coloredlogs.install(level=log_level)

    triggers = TriggerAdapter.fromCSV(
        filename, source_channel, WitheningParam(ar), WdfParam(window_size, window_overlap, snr))

    producer = KafkaProducer({'bootstrap.servers': kafka_broker_host + ":" + kafka_broker_port})

    producer.publish_message(triggers)


if __name__ == '__main__':
    CSVImporter()
