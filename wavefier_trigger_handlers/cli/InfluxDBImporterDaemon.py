import coloredlogs
import click
import logging
import os

from wavefier_trigger_handlers.TriggerSupplier import TriggerSupplier
from wavefier_trigger_handlers.db.InfluxDBImporter import InfluxDBImporter


@click.command()
@click.option('--source-channel', '-sc', default=os.environ["CHANNEL"],
              help="The source channel where the trigger is calculated")
@click.option('--influxdb-host', '-ih', default='influxdb',
              help="The InfluxDB host where store the Trigger")
@click.option('--influxdb-port', '-ip', default=8086,
              help="he InfluxDB port where store the Trigger")
@click.option('--influxdb-db-name', '-idb', default='trigger_report_db',
              help="he InfluxDB database hame where store the Trigger")
@click.option('--influxdb-db-retry-times', '-idbr', default=5,
              help="The Retry time use on InfluxDB insert on database")              
@click.option('--kafka-broker-host', '-kbh', default='kafka',
              help="The kafka broker host where listener the Trigger")
@click.option('--kafka-broker-port', '-kbp', default=29092,
              help="The kafka broker port where listener the Trigger")
@click.option('--kafka-registry-host', '-krh', default='http://kafka-schema-registry',
              help="The kafka registry host where listener the Trigger")
@click.option('--kafka-registry-port', '-krp', default=8081,
              help="The kafka registry port where listener the Trigger")
@click.option('--log-level', default='INFO', show_default=True,
              type=click.Choice(['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']),
              help='Verbose or not verbose computation')
def influxDB_importer_daemon(source_channel, influxdb_host, influxdb_port, influxdb_db_name, influxdb_db_retry_times ,kafka_broker_host,
                             kafka_broker_port, kafka_registry_host, kafka_registry_port, log_level):

    coloredlogs.install(level=log_level)


    logger = logging.getLogger(__name__)

    logger.info('Bootstrap... ')
    logger.info(source_channel) 
    logger.info(influxdb_host)
    logger.info(influxdb_port) 
    logger.info(influxdb_db_name) 
    logger.info(kafka_broker_host)
    logger.info(kafka_broker_port)
    logger.info(kafka_registry_host) 
    logger.info(kafka_registry_port) 
    logger.info(log_level)
    logger.info('Bootstrap... ')
    
    importer = InfluxDBImporter(influxdb_host, influxdb_port, influxdb_db_name)
    trigger_supplier = TriggerSupplier(kafka_broker_host, kafka_broker_port, kafka_registry_host, kafka_registry_port , influxdb_db_retry_times )
    trigger_supplier.addTriggerListener(importer)

    logger.info('Start observing... ')
    trigger_supplier.startObserving()


if __name__ == '__main__':
    influxDB_importer_daemon()
