import logging
import coloredlogs
import click

from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam

from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter
from wavefier_trigger_handlers.db.InfluxDBImporter import InfluxDBImporter
import os


@click.command()
@click.argument('filename', type=click.Path(exists=True))
@click.option('--source-channel', '-sc', default=os.environ["CHANNEL"],  show_default=True,
              help="The source channel where the trigger is calculated")
@click.option('--ar', '-ar', default=3000, show_default=True,
              help="The AR paramenter")
@click.option('--window-size', '-ws', default=1024, show_default=True,
              help="Windows Size")
@click.option('--window-overlap', '-wo', default=16, show_default=True,
              help="Window Overlap")
@click.option('--snr', '-snr', default=4, show_default=True,
              help="the mininum SNR threshould")
@click.option('--influxdb-host', '-ih', default='192.168.178.47', show_default=True,
              help="The InfluxDB host where store the Trigger")
@click.option('--influxdb-port', '-ip', default=8086, show_default=True,
              help="he InfluxDB port where store the Trigger")
@click.option('--influxdb-db-name', '-idb', default='trigger_report_db', show_default=True,
              help="he InfluxDB database hame where store the Trigger")
@click.option('--log-level', default='INFO', show_default=True,
              type=click.Choice(['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']),
              help='Verbose or not verbose computation')
def CSV2Influxdb(filename, source_channel, ar, window_size, window_overlap, snr, influxdb_host, influxdb_port,
               influxdb_db_name, log_level):
    coloredlogs.install(level=log_level)

    triggers = TriggerAdapter.fromCSV(
        filename, source_channel, WitheningParam(ar), WdfParam(window_size, window_overlap, snr))

    importer = InfluxDBImporter(influxdb_host, influxdb_port, influxdb_db_name)

    importer.importTriggers(triggers)


if __name__ == '__main__':
    CSV2Influxdb()
