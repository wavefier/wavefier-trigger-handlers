FROM wdfteam/wdfpipe:2.0.0

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY .  /app

WORKDIR /app

RUN apt-get --force-yes update
#RUN apt-get --assume-yes install librdkafka1
RUN apt-get --assume-yes install librdkafka-dev


#RUN python setup.py install
RUN python setup.py develop

CMD ["python", "-u", "wavefier_trigger_handlers/cli/InfluxDBImporterDaemon.py"]
