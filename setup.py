#!/usr/bin/env python

import wavefier_trigger_handlers
from setuptools import setup, find_packages
from sphinx.setup_command import BuildDoc


cmd_extention = {
    'build_sphinx': BuildDoc
}

prj_name='wavefier_trigger_handlers'

setup(
    name=prj_name,
    author=wavefier_trigger_handlers.__author__,
    version=wavefier_trigger_handlers.__version__,
    description='Project for the management of trigger Wavefier',
    install_requires=[
        'click==7.0',
        'coloredlogs',
        'wavefier-common==2.0.0',
        'influxdb',
    ],
    packages=find_packages(
        exclude=[
            'build',
            'docs',
            'tests',
            'tools',
        ]
    ),
    long_description=open('README.md').read(),
    setup_requires=[
        'nose>=1.0',
    ],
    test_suite='nose.collector',
    dependency_links=[
        "git+https://gitlab+deploy-token-661042:fb4XV93yiAjJXsT3K3f2@gitlab.com/wavefier/wavefier-common.git#egg=wavefier-common-2.0.0"
    ],
    cmdclass=cmd_extention,
    command_options={
        'build_sphinx': {
            'project': ('setup.py', prj_name),
            'version': ('setup.py', wavefier_trigger_handlers.__version__),
            'release': ('setup.py', wavefier_trigger_handlers.__version__),
            'source_dir': ('setup.py', 'docs'),
            'build_dir': ('setup.py', 'docs/_build'),
        }
    },
)
