# Wavefier Trigger Handler 
[![pipeline status](https://gitlab.trust-itservices.com/wavefier/wavefier-trigger-handlers/badges/master/pipeline.svg)](https://gitlab.trust-itservices.com/wavefier/wavefier-trigger-handlers/commits/master) [![coverage report](https://gitlab.trust-itservices.com/wavefier/wavefier-trigger-handlers/badges/master/coverage.svg)](https://gitlab.trust-itservices.com/wavefier/wavefier-trigger-handlers/commits/master)

This module contains all the code to send and receive the Triggers in the wavefier system.



## How To use it
this module is both a command line tool and a framework that can be used within other projects


### CLI - Command line Interface
If you install the package and want to use it as a Command Line Interface, you will have the command `wth-cli`


### Python Module 





## How to develop this modules
Create the docker immage for the develop with the command  ` docker build -t wth-env .`

Run the develop bash with `docker run -it -v $(pwd):/app wth-env /bin/bash`


## Run test
* `python setup.py test` 

## Run Documentation
`make clean && make docs`
